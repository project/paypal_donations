(function ($, Drupal, window, document, undefined) {
  Drupal.behaviors.paypalDonationForms = {
    attach: function (context) {
      // Only number allowed
      $("input[type=text].other").keyup(function() {
        parent_form = $(this).closest("form");
        $(this).val($(this).val().replace(/[^0-9]/g,""));
        // Refresh amount value
        parent_form.find('.amount-holder').val($(this).val());   
      })

      // Checkbox checking
      $('.predefined.donation-amount').click(function() {
        parent_form = $(this).closest("form");
        parent_form.find('input[type=text]').attr('disabled', true);
        parent_form.find('input[type=text]').val('');
        // Refresh amount value
        parent_form.find('.amount-holder').val($(this).val()); 
      });
    
        // Other Checkbox checking
      $('.other-amount.donation-amount').click(function() {
        parent_form = $(this).closest("form");
        parent_form.find('input[type=text]').attr('disabled', false);
        parent_form.find('input[type=text]').val('');
        parent_form.find('.amount-holder').val('');
      });
      // When user click Donate now
      $(".donation-submit-button").click(function() {
        parent_form = $(this).closest("form");
        var post_form = false;
        // If the amount is set set true
        if (parent_form.find('input[type=radio]:checked').length > 0) {
          if ((parent_form.find('.amount-holder').val() !== '') && !isNaN(parent_form.find('.amount-holder').val())) {
            post_form = true;
          }
        }
        if (post_form) {
          parent_form.submit();
        }
        else {
          alert(Drupal.t('Please enter your donation amount'));
        }
      })
    }
  }
})(jQuery, Drupal, this, this.document);