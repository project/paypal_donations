<?php
/**
 * @file
 * PayPal donation Variable API implementation.
 */

/**
 * Implements hook_variable_group_info().
 */
function paypal_donations_variable_group_info() {
  $groups['paypal_donations'] = array(
    'title' => t('PayPal Donations'),
    'description' => t('PayPal Donations variables for Single donations.'),
    'access' => 'administer site configuration',
  );

  $groups['paypal_donations_single'] = array(
    'title' => t('PayPal Donations - Single donation'),
    'description' => t('PayPal Donations variables for Single donations.'),
    'access' => 'administer site configuration',
  );

  $groups['paypal_donations_recurring'] = array(
    'title' => t('PayPal Donations - Recurring donations'),
    'description' => t('PayPal Donations variables for Recurring donations.'),
    'access' => 'administer site configuration',
  );

  return $groups;
}

/**
 * Implements paypal_donations_variable_info().
 */
function paypal_donations_variable_info($options) {
  $variables['paypal_donations_notif_email_subject'] = array(
    'type' => 'string',
    'title' => t('Notification e-mail subject', array(), $options),
    'description' => t('This is the subject for the Notifications e-mail for all donations.', array(), $options),
    'group' => 'paypal_donations',
  );

  $variables['paypal_donations_single_account_email'] = array(
    'type' => 'string',
    'title' => t('PayPal account e-mail (single donations)', array(), $options),
    'description' => t('This is the e-mail address of the PayPal account that will receive Single Donations.', array(), $options),
    'group' => 'paypal_donations_single',
  );

  $variables['paypal_donations_recurring_account_email'] = array(
    'type' => 'string',
    'title' => t('PayPal account e-mail (recurring donations)', array(), $options),
    'description' => t('This is the e-mail address of the PayPal account that will receive Recurring Donations.', array(), $options),
    'group' => 'paypal_donations_recurring',
  );

  $variables['paypal_donations_single_item_name'] = array(
    'type' => 'string',
    'title' => t('PayPal item name (single donations)', array(), $options),
    'description' => t('This is the name to be used as item name on PayPal for Single Donations.', array(), $options),
    'localize' => TRUE,
    'group' => 'paypal_donations_single',
  );

  $variables['paypal_donations_recurring_item_name'] = array(
    'type' => 'string',
    'title' => t('PayPal item name (recurring donations)', array(), $options),
    'description' => t('This is the name to be used as item name on PayPal for Recurring Donations.', array(), $options),
    'localize' => TRUE,
    'group' => 'paypal_donations_recurring',
  );

  $variables['paypal_donations_single_return_url'] = array(
    'type' => 'string',
    'title' => t('Return page URL (single donations)', array(), $options),
    'description' => t('This is the URL of the return page where the user will be sent after paying for Single Donations.', array(), $options),
    'group' => 'paypal_donations_single',
  );

  $variables['paypal_donations_recurring_return_url'] = array(
    'type' => 'string',
    'title' => t('Return page URL (recurring donations)', array(), $options),
    'description' => t('This is the URL of the return page where the user will be sent after paying for Recurring Donations.', array(), $options),
    'group' => 'paypal_donations_recurring',
  );

  $variables['paypal_donations_single_predefined_donation_amounts'] = array(
    'type' => 'string',
    'title' => t('Pre-defined amounts (single donations)', array(), $options),
    'description' => t('These are the pre-defined amounts for Single Donations.', array(), $options),
    'localize' => TRUE,
    'group' => 'paypal_donations_single',
  );

  $variables['paypal_donations_recurring_predefined_donation_amounts'] = array(
    'type' => 'string',
    'title' => t('Pre-defined amounts (single donations)', array(), $options),
    'description' => t('These are the pre-defined amounts for Recurring Donations.', array(), $options),
    'localize' => TRUE,
    'group' => 'paypal_donations_recurring',
  );

  $variables['paypal_donations_single_currency_code'] = array(
    'type' => 'string',
    'title' => t('Currency code (single donations)', array(), $options),
    'description' => t('This is currency code for Single Donations.', array(), $options),
    'localize' => TRUE,
    'group' => 'paypal_donations_single',
  );

  $variables['paypal_donations_recurring_currency_code'] = array(
    'type' => 'string',
    'title' => t('Currency code (recurring donations)', array(), $options),
    'description' => t('This is currency code for Recurring Donations.', array(), $options),
    'localize' => TRUE,
    'group' => 'paypal_donations_recurring',
  );

  $variables['paypal_donations_single_currency_sign'] = array(
    'type' => 'string',
    'title' => t('Currency sign (single donations)', array(), $options),
    'description' => t('This is currency sign for Single Donations.', array(), $options),
    'localize' => TRUE,
    'group' => 'paypal_donations_single',
  );

  $variables['paypal_donations_recurring_currency_sign'] = array(
    'type' => 'string',
    'title' => t('Currency sign (recurring donations)', array(), $options),
    'description' => t('This is currency sign for Recurring Donations.', array(), $options),
    'localize' => TRUE,
    'group' => 'paypal_donations_recurring',
  );

  $variables['paypal_donations_single_submit_value'] = array(
    'type' => 'string',
    'title' => t('Donation submit text (single donations)', array(), $options),
    'description' => t('This is the text used in the Donation submit link for Single Donations.', array(), $options),
    'localize' => TRUE,
    'group' => 'paypal_donations_single',
  );

  $variables['paypal_donations_recurring_submit_value'] = array(
    'type' => 'string',
    'title' => t('Donation submit text (recurring donations)', array(), $options),
    'description' => t('This is the text used in the Donation submit link for for Recurring Donations.', array(), $options),
    'localize' => TRUE,
    'group' => 'paypal_donations_recurring',
  );

  $variables['paypal_donations_single_custom_amount_allowed'] = array(
    'type' => 'string',
    'title' => t('Custom Amount switch (single donations)', array(), $options),
    'description' => t('This enables or disables the Custom Amount for Single Donations.', array(), $options),
    'group' => 'paypal_donations_single',
  );

  $variables['paypal_donations_recurring_custom_amount_allowed'] = array(
    'type' => 'string',
    'title' => t('Custom Amount switch (recurring donations)', array(), $options),
    'description' => t('This enables or disables the Custom Amount for Recurring Donations.', array(), $options),
    'group' => 'paypal_donations_recurring',
  );

  $variables['paypal_donations_single_custom_amount_label'] = array(
    'type' => 'string',
    'title' => t('Custom amount field label (single donations)', array(), $options),
    'description' => t('This is the label for the Custom Amount field for Single Donations.', array(), $options),
    'localize' => TRUE,
    'group' => 'paypal_donations_single',
  );

  $variables['paypal_donations_recurring_custom_amount_label'] = array(
    'type' => 'string',
    'title' => t('Custom amount field label (recurring donations)', array(), $options),
    'description' => t('This is the label for the Custom Amount field for Recurring Donations.', array(), $options),
    'localize' => TRUE,
    'group' => 'paypal_donations_recurring',
  );

  $variables['paypal_donations_single_notification_email'] = array(
    'type' => 'text',
    'title' => t('User notification e-mail (single donations)', array(), $options),
    'description' => t('This is the text for the notification e-mail related to Single Donations.', array(), $options),
    'localize' => TRUE,
    'group' => 'paypal_donations_single',
  );

  $variables['paypal_donations_recurring_notification_email'] = array(
    'type' => 'text',
    'title' => t('User notification e-mail (recurring donations)', array(), $options),
    'description' => t('This is the text for the notification e-mail related to Recurring Donations.', array(), $options),
    'localize' => TRUE,
    'group' => 'paypal_donations_recurring',
  );

  $variables['paypal_donations_recurring_period'] = array(
    'type' => 'string',
    'title' => t('Recurring period (interval)', array(), $options),
    'description' => t('This is the recurring interval for the donation to happen (i.e.: every <strong>3</strong> months).', array(), $options),
    'group' => 'paypal_donations_recurring',
  );

  $variables['paypal_donations_recurring_unit'] = array(
    'type' => 'string',
    'title' => t('Recurring unit of time', array(), $options),
    'description' => t('This is the unit of time to be used with the period/interval for recurring donations.', array(), $options),
    'group' => 'paypal_donations_recurring',
  );

  return $variables;
}
